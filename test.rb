class MyComplexBusinessProcess
	def initialize args
		@log = []
		@step1 = args_for_step_1
		@step2 = args_for_step_2
		#...
	end

	def process?
		step1 && step2 && step3 && step4 #&& ... more steps
	end

	def step1
		#...
		ok = step.success?
		log << step.explanation unless ok
		ok
	end

	def step2
		#...
		ok = step.success?
		log << step.explanation unless ok
		ok
	end

	def step3
		#...
		ok = step.success?
		log << step.explanation unless ok
		ok
	end

	def step4
		#...
		ok = step.success?
		log << step.explanation unless ok
		ok
	end
	#... more steps
end